<?php
session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}


require 'user_gurusmp.php';
require 'koneksi.php';
use UserGuruSMP\UserGuru;

$obj = new UserGuru();
?>
<head>
  <br>
  <br>
<title>DATA GURU SMP</title>
  <meta charset="UTF-8">
</head>
<link rel="stylesheet" type="text/css" href="css/tabel.css">
<link rel="stylesheet" href="css/nihh.css">
<link rel="stylesheet" href="css/smp.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
<header>
<div class="w3-top">
    
    <div class="w3-bar w3-teal1 w3-card w3-left-align w3-large ">
    <a href="guru.php" class="w3-bar-item1 w3-button w3-padding-large ">Kembali</a> 
    <a href ="index.php" class="w3-bar-item w3-button w3-padding-large">Home</a>

    <form action = "input_guru.php" method ="POST" name="login">
<input type ="submit" class="w3-bar-item w3-button w3-padding-large" name = "submit" value = "Input Data Guru SMP">


</div>
</header>
<h1>Guru SMP Harapan Bangsa</h1>
<p>
<center>
<table class= 'table table-bordered table-responsive'> 
<div class="container">
 <tr>
     <th>NO</th>
     <th>NIG</th>
     <th>Nama</th>
     <th>Kelas</th>
     <th>Tingkatan</th>
     <th>Tgl_Lahir</th>
     <th>JK</th>
     <th>Alamat</th>
     <th>Mapel</th>
     <th colspan="5">aksi</th>
 </tr>
 </div>
</center>
 <?php 
$no=1;
	$data=$obj->showData();
	if($data->rowCount()>0){
	while($row=$data->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
	<td><?php echo $no; ?></td>
	<td><?php echo $row['NIG']; ?></td>
	<td><?php echo $row['nama']; ?></td>
    <td><?php echo $row['kelas']; ?></td>
    <td><?php echo $row['Tingkatan']; ?></td>
    <td><?php echo $row['tgl_lahir']; ?></td>
    <td><?php echo $row['jk']; ?></td>
    <td><?php echo $row['alamat']; ?></td>
    <td><?php echo $row['mapel']; ?></td>
   
    <td><a href="editguru.php?NIG=<?php echo $row['NIG']; ?>">Edit</a></td>
    <td><a href="proses_delete_guru.php?NIG=<?php echo $row['NIG']; ?>">Hapus</a></td>
    </tr>
    </form>
<?php 
$no+=1; } 
$data->closeCursor();
}else{
echo '<tr>
		<td> Not found</td>	
    </tr>';
}
?>
     
