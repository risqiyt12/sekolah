<?php 

session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}


require 'userix.php';
require 'koneksi.php';
use Userix\User;

$obj = new User();
?>
<link rel="stylesheet" type="text/css" href="css/tabel.css">
<link rel="stylesheet" href="css/nihh.css">
<link rel="stylesheet" href="css/smp.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
<div class="w3-top">
    
    <div class="w3-bar w3-teal1 w3-card w3-left-align w3-large ">
    <a href="smp.php" class="w3-bar-item1 w3-button w3-padding-large ">Kembali</a> 
    <a href ="index.php" class="w3-bar-item w3-button w3-padding-large">Home</a>
    <a href="siswa_vii.php" class="w3-bar-item w3-button w3-padding-large ">Siswa Kelas VII </a>
    <a href="siswa_viii.php" class="w3-bar-item w3-button w3-padding-large ">Siswa Kelas VIII </a>
    <a href="siswa_ix.php" class="w3-bar-item w3-button w3-padding-large ">Siswa Kelas IX </a>

<form action = "input_siswa_ix.php" method ="POST" name="login">
<input type ="submit" class="w3-bar-item w3-button w3-padding-large" name = "submit" value = "Input Data Siswa SMP Kelas IX">

</div>
<h1>Siswa Kelas IX SMP Harapan Bangsa</h1>
<p>
<center>
<table class= 'table table-bordered table-responsive'>  
<div class="container">
    <tr>
     <th>NO</th>
     <th>NIG</th>
     <th>Nama</th>
     <th>Kelas</th>
     <th>Tgl_Lahir</th>
     <th>JK</th>
     <th>Alamat</th>
     <th  colspan="5">aksi</th>
 </tr>
</center>
 
 <?php 
$no=1;
	$data=$obj->showDataix();
	if($data->rowCount()>0){
	while($row=$data->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
    
	<td><?php echo $no; ?></td>
	<td><?php echo $row['NIG']; ?></td>
	<td><?php echo $row['nama']; ?></td>
    <td><?php echo $row['kelas']; ?></td>
    <td><?php echo $row['tgl_lahir']; ?></td>
    <td><?php echo $row['jk']; ?></td>
    <td><?php echo $row['alamat']; ?></td>
   
    <td><a href="editguru.php?NIG=<?php echo $row['NIG']; ?>">Edit</a></td>
    <td><a href="proses_delete_guru.php?NIG=<?php echo $row['NIG']; ?>">Hapus</a></td>
    </tr>
    </form>
<?php 
$no+=1; } 
$data->closeCursor();
}else{
echo '<tr>
		<td> Not found</td>	
    </tr>';
}
?>
 <h1>NAMA WALIKELAS</h1>
 <center>
<table class= 'table table-bordered table-responsive'>  
 <table border="1" width="1300px" >
    <tr>
     <th>NO</th>
     <th>NIS</th>
     <th>Nama</th>
     <th>Kelas</th>
     <th>Tgl_Lahir</th>
     <th>JK</th>
     <th>Alamat</th>
     <th  colspan="5">aksi</th>
 </tr>
</center>
 
 <?php 
$no=1;
	$data=$obj->showData();
	if($data->rowCount()>0){
	while($row=$data->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
    
	<td><?php echo $no; ?></td>
	<td><?php echo $row['NIS']; ?></td>
	<td><?php echo $row['nama']; ?></td>
    <td><?php echo $row['kelas']; ?></td>
    <td><?php echo $row['tgl_lahir']; ?></td>
    <td><?php echo $row['jk']; ?></td>
    <td><?php echo $row['alamat']; ?></td>
   
    <td><a href="editsmp.php?NIS=<?php echo $row['NIS']; ?>">Edit</a></td>
    <td><a href="prosesdelete.php?NIS=<?php echo $row['NIS']; ?>">Hapus</a></td>
    </tr>
    </form>
<?php 
$no+=1; } 
$data->closeCursor();
}else{
echo '<tr>
		<td> Not found</td>	
    </tr>';
}
?>
 <h1>NAMA SISWA</h1>


