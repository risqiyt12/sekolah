<?php

session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}

require 'usermapel.php';
require 'koneksi.php';

use UserMapel\UserMapel;

$obj = new UserMapel();
?>
<link rel="stylesheet" type="text/css" href="css/tabel.css">
<link rel="stylesheet" href="css/nihh.css">
<link rel="stylesheet" href="css/smp.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
    <header>
<div class="w3-top">
    
    <div class="w3-bar w3-teal1 w3-card w3-left-align w3-large ">
      <a href="index.php" class="w3-bar-item w3-button w3-padding-large ">Kembali</a>
<form action = "input_mapelsma.php" method ="POST" name="login">
<input type ="submit" class="w3-bar-item w3-button w3-padding-large" name = "submit" value = "Input Data Mapel">

</div>
</header>
<br>
<br>
<br>
<h1>DATA SEMUA MAPEL HARAPAN BANGSA</h1>
<center>
<table class= 'table table-bordered table-responsive'> 
<div class="container">
    <tr>
     <th>NO</th>
     <th>Mapel</th>
     <th colspan="15">aksi</th>
 </tr>
</center>
 <?php 
$no=1;
	$data=$obj->showDataMapel();
	if($data->rowCount()>0){
	while($row=$data->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
	<td><?php echo $no; ?></td>
	<td><?php echo $row['mapel']; ?></td>
    <td><a href="editmapel.php?mapel=<?php echo $row['mapel']; ?>">Edit</a></td>
    <td><a href="proses_delete_mapel.php?mapel=<?php echo $row['mapel']; ?>">Hapus</a></td>
    </tr>
    </form>
<?php 
$no+=1; } 
$data->closeCursor();
}else{
echo '<tr>
		<td> Not found</td>	
    </tr>';
}
