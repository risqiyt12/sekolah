<?php

session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}

require 'user_sma.php';
require 'koneksi.php';
use UserSma\UserSma;

$obj = new UserSma();
?>

<link rel="stylesheet" type="text/css" href="css/tabel.css">
<link rel="stylesheet" href="css/nihh.css">
<link rel="stylesheet" href="css/smp.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
<div class="w3-top">
    
    <div class="w3-bar w3-teal1 w3-card w3-left-align w3-large ">
      <a href="index.php" class="w3-bar-item w3-button w3-padding-large ">Kembali</a>
      <div class="dropdown">   
      <button class="dropbtn">Kelas SMA
                        <i class="fa fa-caret-down"></i>
                      </button>
                      <div class="dropdown-content">
                        <div class="row">
                          <div class="column">
                            <h3>KELAS IPA</h3>
                            <a href="siswa_x_ipa.php">X</a>
                            <a href="siswa_xi_ipa.php">XI</a>
                            <a href="siswa_xii_ipa.php">XII</a>
                          </div>
                          <div class="column">
                              <h3>KELAS IPS</h3>
                              <a href="siswa_x_ips.php">X</a>
                              <a href="siswa_xi_ips.php">XI</a>
                              <a href="siswa_xii_ips.php">XII</a>
                            </div>
                          </div>
                        </div>
                      </div>


</div>
<h1>Siswa SMA Harapan Bangsa</h1>
<center>
<table class= 'table table-bordered table-responsive'> 
<div class="container">

 <tr>
    <tr>
     <th>NO</th>
     <th>NIS</th>
     <th>Nama</th>
     <th>Kelas</th>
     <th>Tgl_Lahir</th>
     <th>JK</th>
     <th>Alamat</th>
     <th>nama_jurusan</th>
     <th colspan="5">aksi</th>
 </tr>
 </div>
</center>
 <?php 
$no=1;
	$data=$obj->showData();
	if($data->rowCount()>0){
	while($row=$data->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
	<td><?php echo $no; ?></td>
	<td><?php echo $row['NIS']; ?></td>
	<td><?php echo $row['nama']; ?></td>
    <td><?php echo $row['kelas']; ?></td>
    <td><?php echo $row['tgl_lahir']; ?></td>
    <td><?php echo $row['jk']; ?></td>
    <td><?php echo $row['alamat']; ?></td>
    <td><?php echo $row['nama_jurusan']; ?></td>
    <td><a href="editsma.php?NIS=<?php echo $row['NIS']; ?>">Edit</a></td>
        <td><a href="proses_delete_sma.php?NIS=<?php echo $row['NIS']; ?>">Hapus</a></td>
    </tr>
    </form>
<?php 
$no+=1; } 
$data->closeCursor();
}else{
echo '<tr>
		<td> Not found</td>	
    </tr>';
}
?>

    <form action="" method="post">
        <label></label>
        <select  name="category" onChange="document.location.href=this.options[this.selectedIndex].value;">
        <option value="">-- Pilih Kelas & Jurusan -- </option>
        <option value="siswa_x_ipa.php?XIPA">X IPA</option>
        <option value="siswa_x_ips.php?XIPS">X IPS</option>
        <option value="siswa_xi_ipa.php?XIPA">XI IPA</option>
        <option value="siswa_xii_ips.php?XIPS">XI IPS</option>
        <option value="siswa_xii_ipa.php?XIPA">XII IPA</option>
        <option value="siswa_xii_ips.php?XIPS">XII IPS</option>
 </select>