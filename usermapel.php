<?php 
namespace UserMapel;

use Koneksi\Koneksi;
use PDOException;
class UserMapel 
{
    public function __construct()
    {
       
    }


    public function showDataMapel(){
        $conn = new Koneksi();
        $db=$conn->metal();
		$stmt=$db->prepare("SELECT * FROM mapel");
		$stmt->execute(); 
		return $stmt;
    }

    public function insertDataMapel($mapel)
	{
        if(isset($_POST['input']))
		{
                $conn = new Koneksi();
                $db=$conn->metal();
                $sql="INSERT INTO mapel (mapel) VALUES (:mapel)";
                $stmt=$db->prepare($sql);
				$stmt->bindParam(":mapel", $mapel);            
				$stmt->execute();
				return true;
                header("location:mapelsmp.php");
		}
        
    }

    public function delete($mapel)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $sql ="DELETE FROM mapel WHERE mapel = :mapel";
        $stmt =$db->prepare($sql);
         $stmt->bindParam(":mapel", $mapel);
         $stmt->execute();
        return true;
        header("location:datamapel.php");
	}

    public function editMapel($mapel)
    {
        if(isset($_POST['edit']))
        {
            $mapel = $_POST['mapel'];
        }try {
            $conn = new Koneksi();
            $db=$conn->metal();
            $query = "UPDATE mapel SET mapel=:mapel WHERE mapel = :mapel";
            $statement = $db->prepare($query);
            $data = [
                ':mapel' => $mapel
            ];
            $query_execute = $statement->execute($data);
            if($query_execute)
            {
                header('Location:datamapel.php');
                exit(0);
            }
            else
            {
                header('Location:datamapel.php');
                exit(0);
            }
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}