<?php
require 'usermapel.php';
require 'koneksi.php';

use Koneksi\Koneksi;
use UserMapel\UserMapel;

$edit = new UserMapel();

?>

<head>
    <title>Edit Siswa</title>
</head>
<link rel="stylesheet" type="text/css" href="css/smp.css">
<body>
   
<?php

if(!isset($_GET['mapel'])){
    die("Error: ID Tidak Dimasukkan");
}

//Ambil data
$conn = new Koneksi();
$db=$conn->metal();
$query = $db->prepare("SELECT * FROM mapel WHERE mapel = :mapel");
$query->bindParam(":mapel", $_GET['mapel']);
// Jalankan perintah sql
$query->execute();
if($query->rowCount() == 0){
    // Tidak ada hasil
    die("Error: ID Tidak Ditemukan");
}else{
    // ID Ditemukan, Ambil data
    $data = $query->fetch();
}


?>
    
    <form action="proses_edite_mapel.php" method="POST" name="edit">
        <center>
        <table>
        <h1>EDIT MAPEL</h1>
        <tr>
            <td>
                <label>Nama Mapel:</label>
            </td>
            <td>
                <input type="text" name="mapel" value="<?=$data['mapel']; ?>"/>
            </td>
        </tr>

        </center>
        </table>
        <input type="submit" class="tombol1" name="submit" value="edit" />
    </form>
</body>
</html>
        <form action = "datamapel.php" method ="POST" name="kembali">
        <input type ="submit" class="tombol1" name = "submit" value = "Batal">
        </form>
