<?php
session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>DATA SEKOLAH HARAPAN BANGSAT</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/nihh.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!-- Navbar -->
<div class="w3-top">
    
  <div class="w3-bar w3-teal w3-card w3-left-align w3-large ">
    <div class="navbar">
    
        <div class="dropdown">
          <button class="dropbtn">Data Smp
            <i class="fa fa-caret-down"></i>
          </button>
          <div class="dropdown-content">
            <div class="row">
              <div class="column">
                <h3>Kelas</h3>
                <a href="siswa_vii.php">VII</a>
                <a href="siswa_viii.php">VIII</a>
                <a href="siswa_ix.php">IX</a>
              
              </div>
              <div class="column">
                      <h3>Data Semua Kelas</h3>
                      <a href="smp.php">Data semua kelas</a>
                    </div>
              </div>
            </div>
          </div>
          <div class="dropdown">
      
              <button class="dropbtn">Data Sma
                <i class="fa fa-caret-down"></i>
              </button>
              <div class="dropdown-content">
                <div class="row">
                  <div class="column">
                 
                    <h3>Kelas IPA</h3>
                    <a href="siswa_x_ipa.php">X IPA</a>
                    <a href="siswa_xi_ipa.php">XI IPA</a>
                    <a href="siswa_xii_ipa.php">XII IPA</a>
                 
                  </div>
                  <div class="column">
                      <h3>Kelas IPS</h3>
                      <a href="siswa_x_ips.php">X IPS</a>
                      <a href="siswa_xi_ips.php">XI IPS</a>
                      <a href="siswa_xii_ips.php">XII IPS</a>
                    </div>
                    <div class="column">
                      <h3>Data Semua Kelas</h3>
                      <a href="sma.php">Data semua kelas</a>
                    </div>
                  </div>
                </div>
      
              </div>
              <div class="dropdown">
      
                  <button class="dropbtn">Data Guru
                    <i class="fa fa-caret-down"></i>
                  </button>
                  <div class="dropdown-content">
                    <div class="row">
                      <div class="column">
                        <h3>Data Guru SMP</h3>
                        <a href="gurusmp.php">Guru SMP</a>
                      </div>
                      <div class="column">
                        <h3>Data Guru SMA</h3> 
                        <a href="gurusma.php">Guru SMA</a>
                      </div>
                      <div class="column">
                      <h3>Data Semua Guru
                      </h3>
                      <a href="guru.php">Data Semua Guru</a>
                    </div>
                      </div>
                    </div>
                  </div>
                  <a href ="datamapel.php" class="w3-bar-item w3-button w3-padding-large">Data Mapel</a>
                  <a href="logout.php" class="w3-bar-item1 w3-button w3-padding-large ">Logout</a> 
                          </div>
        </div> 
        
      </div>
  

<!-- Header -->
<header class="w3-container w3-red w3-center" style="padding:500px 16px">
<img src=img/pdibiru.png>
  <h1 class="w3-margin w3-jumbo">SELAMAT DATANG DI HALAMAN ADMIN</h1>
  <p class="w3-xlarge">Silahkan click tombol diatas untuk penginputan data</p>

</header>


</body>
</html>
