<?php 

session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}

require 'usermapel.php';
require 'koneksi.php';
require 'user_guru.php';
require 'user_smp.php';
use UserSmp\UserSmp;
use UserMapel\UserMapel;

$guru = new UserMapel();
$smp = new UserSmp();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Input GURU SMP</title>
    <link rel="stylesheet" type="text/css" href="css/smp.css">
</head>
<body>
    <form action="proses_insert_guru.php" method="POST" name="insert">
        <div class="kotak_login">
            <h1>INPUT GURU SMP</h1>
            <div>
            <table border="0" align="center">
        <tr>
            <td>
                <label>NIG:</label>
            </td>
            <td>
                <input type="text" name="NIG"  />
            </td>
        </tr>
        <tr>
            <td>
                <label>Nama:</label>
            </td>
            <td>
                <input type="text" name="nama"  />
            </td>
        </tr>
        <tr>
            <td>
                <label for="kelas">Pilih Kelas :</label>
            </td>
            <td>
                <select id="kelas" name="kelas">
                <option disabled selected>pilih</option>
                <option value="VII">VII</option>
                <option value="VIII">VIII</option>
                <option value="IX">IX</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <label for="Tingkatan">Tingkatan:</label>
            </td>
            <td>
            <input type="checkbox" id="Tingkatan" name="Tingkatan" value="SMP" checked>
            <label for="Tingkatan"> SMP </label>
            </td>
        </tr>
        <tr>
            <td>
               <label>Tanggal Lahir:</label>
            </td>
            <td>
                <input type="date" name="tgl_lahir"  />
            </td>
        </tr>
        <tr>
            <td>
                <label for="jk">Pilih Jenis Kelamin:</label>
            </td>
            <td>
                <select id="jk" name="jk">
                <option disabled selected>pilih</option>
                <option value="laki-laki">Laki-laki</option>
                <option value="perempuan">Perempuan</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <label>Alamat:</label>
            </td>
            <td>
                <textarea name="alamat"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <label for="nama_jurusan">Pilih Jurusan:</label>
            </td>
            <td>
            <input type="checkbox" id="nama_jurusan" name="nama_jurusan" value="TIDAK ADA " checked>
            <label for="nama_jurusan"> TIDAK ADA </label>
            </td>
        </tr>
        <tr>
            <td>
                <label for="mapel">Mapel:</label>
            </td>
            <td>
                <select id="mapel" name="mapel">
                <option disabled selected>pilih</option>
            <?php 
            $data=$guru->showDataMapel();
            foreach ($data as $row) {
            ?>
                <option value="<?php echo $row['mapel']; ?>"><?php echo $row['mapel']; ?></option>
            <?php 
            }
            ?>
             </td>
        </tr>
        <tr>
            <td>
                <td>
                <form>
                <input type="submit" class="tombol_input" name="input" value="input" />
                </form>
                </td>
            </td>
        </tr>
        <tr>
            <td>
                <td>
                <form action = "guru.php" method ="POST" name="kembali">
                    <input type ="submit" class="tombol_input" name = "submit" value = "kembali">
                </form>
                </td>
            </td>
        </tr>
        </table>
        </form>
        </div>
        </div>
</body>
</html>

