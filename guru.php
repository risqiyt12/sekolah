<?php
session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}


require 'user_guru.php';
require 'koneksi.php';
use UserGuru\UserGuru;

$obj = new UserGuru();
?>
<head>
<br>
<br>
<br>
  <title>DATA GURU</title>
  <meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/tabel.css">
<link rel="stylesheet" href="css/nihh.css">
<link rel="stylesheet" href="css/smp.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
  <header>
<div class="w3-top">
    
    <div class="w3-bar w3-teal1 w3-card w3-left-align w3-large ">
      <div class="navbar">
      <a href="index.php" class="w3-bar-item w3-button w3-padding-large ">Home</a>

    <div class="dropdown">
      <button class="dropbtn">Data SMP
        <i class="fa fa-caret-down"></i>
      </button>
      <div class="dropdown-content">
        <div class="row">
          <div class="column">
            <h3>Kelas</h3>
            <a href="siswa_vii.php">VII</a>
            <a href="siswa_viii.php">VIII</a>
            <a href="siswa_ix.php">IX</a>
          
          </div>
          <div class="column">
                  <h3>Data Semua Kelas</h3>
                  <a href="smp.php">Data semua kelas</a>
                </div>
          </div>
        </div>
      </div>
      <div class="dropdown">
  
          <button class="dropbtn">Data SMA
            <i class="fa fa-caret-down"></i>
          </button>
          <div class="dropdown-content">
            <div class="row">
              <div class="column">
             
                <h3>Kelas IPA</h3>
                <a href="siswa_x_ipa.php">X IPA</a>
                <a href="siswa_xi_ipa.php">XI IPA</a>
                <a href="siswa_xii_ipa.php">XII IPA</a>
             
              </div>
              <div class="column">
                  <h3>Kelas IPS</h3>
                  <a href="siswa_x_ips.php">X IPS</a>
                  <a href="siswa_xi_ips.php">XI IPS</a>
                  <a href="siswa_xii_ips.php">XII IPS</a>
                </div>
                <div class="column">
                  <h3>Data Semua Kelas</h3>
                  <a href="sma.php">Data semua kelas</a>
                </div>
              </div>
            </div>
         </div>
<form action = "input_guru.php" method ="POST" name="login">
<input type ="submit" class="w3-bar-item w3-button w3-padding-large" name = "submit" value = "Input Data Guru SMP">
</form>
<form action = "input_guru_sma.php" method ="POST" name="login">
<input type ="submit" class="w3-bar-item w3-button w3-padding-large" name = "submit" value = "Input Data Guru SMA">
</form>
</div>
</div>
</header>
<h1>Guru SMA dan SMP Harapan Bangsa</h1>
<p>
<center>
<table class= 'table table-bordered table-responsive'> 
<div class="container"
    <tr>
     <th>NO</th>
     <th>NIG</th>
     <th>Nama</th>
     <th>Kelas</th>
     <th>Tingkatan</th>
     <th>Tgl_Lahir</th>
     <th>JK</th>
     <th>Alamat</th>
     <th>Jurusan</th>
     <th>Mapel</th>
     <th colspan="5">aksi</th>
 </tr>
</center>
 <?php 
$no=1;
	$data=$obj->showData();
	if($data->rowCount()>0){
	while($row=$data->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
	<td><?php echo $no; ?></td>
	<td><?php echo $row['NIG']; ?></td>
	<td><?php echo $row['nama']; ?></td>
    <td><?php echo $row['kelas']; ?></td>
    <td><?php echo $row['Tingkatan']; ?></td>
    <td><?php echo $row['tgl_lahir']; ?></td>
    <td><?php echo $row['jk']; ?></td>
    <td><?php echo $row['alamat']; ?></td>
    <td><?php echo $row['nama_jurusan']; ?></td>
    <td><?php echo $row['mapel']; ?></td>
   
    <td><a href="editguru.php?NIG=<?php echo $row['NIG']; ?>">Edit</a></td>
    <td><a href="proses_delete_guru.php?NIG=<?php echo $row['NIG']; ?>">Hapus</a></td>
    </tr>
    </form>
<?php 
$no+=1; } 
$data->closeCursor();
}else{
echo '<tr>
		<td> Not found</td>	
    </tr>';
}
?>
</body>
 

    